package com.dhruva.gitlabstarterproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabStarterProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitlabStarterProjectApplication.class, args);
	}

}
